
var _map = null;
var _track = null;
var _my_marker = null;
var _my_accuracy = 3000;
var _is_online = false;
var _is_tracking = false;
var _users_to_follow = [];
var _follow_last_update = new Date(0);
var _follow_lock = null;
var _tracks = {};
var _last_known_locations = {};
var _pois = [];

// List of the available tile layers
var _tileLayers = {
	"OSM-FR": L.tileLayer('http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
		attribution: 'Map Data:&nbsp;<a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap contributors</a>. Rendering:&nbsp;<a href="http://www.openstreetmap.fr/">&copy; OSM-FR</a>',
	}),
	"Wikimedia": L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
		attribution: 'Map Data:&nbsp;<a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap contributors</a>. Rendering:&nbsp;<a href="http://www.wikimedia.org/">&copy; Wikimedia</a>',
	}),
	"Mapnik (international)": L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: 'Map Data:&nbsp;<a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap contributors</a>. Rendering:&nbsp;Mapnik',
	}),
	"Cyclisme": L.tileLayer('http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', {
		attribution: 'Map Data:&nbsp;<a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap contributors</a>. Rendering:&nbsp;<a href="http://www.thunderforest.com/">&copy; OpenCycleMap</a>',
	}),
	"Relief": L.tileLayer('http://{s}.tile3.opencyclemap.org/landscape/{z}/{x}/{y}.png', {
		attribution: 'Map Data:&nbsp;<a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap contributors</a>. Rendering:&nbsp;<a href="http://www.thunderforest.com/">&copy; OpenCycleMap</a>',
	}),
	"Transports": L.tileLayer('http://{s}.tile2.opencyclemap.org/transport/{z}/{x}/{y}.png', {
		attribution: 'Map Data:&nbsp;<a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap contributors</a>. Rendering:&nbsp;<a href="http://www.thunderforest.com/">&copy; OpenCycleMap</a>',
	}),
};

var _prefs = {
	follow_mode: "position", // Possible values: 'track', 'position', 'free'
	tile_layer_name: "OSM-FR",
}


function init_map() {
	// Build the initial map.
	_map = L.map('map').setView([48.8577, 2.4], 11);
	_map.addLayer(_tileLayers[_prefs.tile_layer_name]);

	_track = L.polyline([], {smoothFactor: 3}).addTo(_map);
	_my_marker = L.circleMarker([48.8577, 2.4], {
		color: 'transparent',
		radius: 10,
	}).addTo(_map);

	_map.on('locationfound', locationfound);
	_map.on('locationerror', locationerror);
	_map.on('contextmenu', contextmenu);
	_map.on('zoomend', function () {
		if (_my_marker && _my_accuracy) {
			_my_marker.setRadius(_compute_radius(_my_accuracy));
		}
	});
}

/**
 * Help the user know about the accuracy of his current location.
 * A large circle means a poor precision, a smal circle means good precision.
 */
function _compute_radius(accuracy) {
		var factor = (1700000000000 / Math.pow(_map.getZoom(), 10)) + 0;
		var res = accuracy / factor;
		return Math.max(5, res);
}


function adjust_map_view() {
	if (_prefs.follow_mode == 'track' && $('#map').is(':visible')) {
		_map.fitBounds(_track.getBounds());
	} else if (_prefs.follow_mode == 'position' && $('#map').is(':visible')) {
		var points_to_display_count = Object.keys(_last_known_locations).length + _pois.length;
		if (points_to_display_count == 1) {
			var latlng = _last_known_locations[Object.keys(_last_known_locations)[0]]
			_map.panTo(latlng);
		} else if (points_to_display_count > 1) {
			var view_bounds = L.latLngBounds([]);
			for (k in _last_known_locations) {
				view_bounds.extend(_last_known_locations[k]);
			}
			for (var i = 0; i < _pois.length; i++) {
				var poi = _pois[i];
				view_bounds.extend(poi.getLatLng());
			}
			if (view_bounds.isValid()) {
				_map.fitBounds(view_bounds, {padding: [50, 50]});
			}
		} else {
			console.error("Nothing to center map on.", _last_known_locations);
		}
	} else if (_prefs.follow_mode == 'free') {
		// don't change current map view
	}
}


function switch_layer(layername) {
	var new_layer = _tileLayers[layername];
	if (!new_layer) {
		console.error("Failed to switch to layer \"" + layername + "\".\nPossible layer names are: ", Object.keys(_tileLayers));
		return false;
	}
	var current_layer = null;
	_map.eachLayer(function (l) { if ('_tiles' in l) { current_layer = l;}});
	_map.addLayer(new_layer);
	if (current_layer) {
		_map.removeLayer(current_layer);
	}
	_prefs.tile_layer_name = layername;
}


function locationerror(e) {
	_map.stopLocate();
	window.navigator.vibrate([500, 100, 500]);
	notify("Impossible de trouver votre localisation au bout de 5min.\nExtinction du GPS pour économiser la batterie.\n\n" + e.message);
	_is_tracking = false;
	$('#btn-toggle-tracking').text('Partager ma position');
}


function locationfound(pos) {
	pos = _pos_to_json(pos);
	// Refresh the map
	var latlon = L.latLng([pos.coords.latitude, pos.coords.longitude]);
	_my_marker.setLatLng(latlon).setStyle({color: 'blue'});
	_my_accuracy = pos.coords.accuracy;
	_my_marker.setRadius(_compute_radius(pos.coords.accuracy));
	_track.addLatLng(latlon);

	_last_known_locations[get_username()] = latlon;
	adjust_map_view();

	// store position and send it to server ASAP
	store_pos(pos);
	if (_is_online) {
		upload_track();
	}
}


function contextmenu(evt) {
	var latlng = evt.latlng;
	var popup = L.popup({
			closeButton: false,
			minWidth: 200,
		})
		.setLatLng(latlng)
		.setContent(
			'<div data-lat="' + latlng.lat + '" data-lng="' + latlng.lng + '">'+
			'<button id="btn-set-current-loc" class="ui-btn">Je suis ici</button>' +
			'<button id="btn-set-marker-loc"  class="ui-btn">Marqueur</button>' +
			'</div>')
		.openOn(_map);
}


function store_pos(pos) {
	// _track_log is an array of pos objects of the current track recording
	// var _track_log = localStorage.getItem("_track_log");
	// if (_track_log) {
		// _track_log = JSON.parse(_track_log);
	// } else {
		// _track_log = [];
	// }
	// _track_log.push(pos);
	// localStorage.setItem("_track_log", JSON.stringify(_track_log));

	// _pos_to_sync is an array of positions not yet uploaded to server
	var _pos_to_sync = sessionStorage.getItem("_pos_to_sync");
	if (_pos_to_sync) {
		_pos_to_sync = JSON.parse(_pos_to_sync);
	} else {
		_pos_to_sync = [];
	}
	_pos_to_sync.push(pos);
	sessionStorage.setItem("_pos_to_sync", JSON.stringify(_pos_to_sync));
}


function _pos_to_json(pos) {
	return {
		"timestamp": new Date(pos.timestamp).toJSON(),
		"coords": {
			"latitude": pos.latlng != undefined ? pos.latlng.lat : pos.latitude != undefined ? pos.latitude : pos.coords.latitude,
			"longitude": pos.latlng != undefined ? pos.latlng.lng : pos.longitude != undefined ? pos.longitude : pos.coords.longitude,
			"accuracy": pos.accuracy != undefined ? pos.accuracy : pos.coords.accuracy,
//			"altitude": pos.altitude != undefined ? pos.altitude : pos.coords.altitude != undefined ? pos.coords.altitude : 0,
//			"altitudeAccuracy": pos.altitudeAccuracy != undefined ? pos.altitudeAccuracy : pos.coords.altitudeAccuracy != undefined ? pos.coords.altitudeAccuracy : -1,
//			"speed": pos.speed != undefined ? pos.speed : pos.coords.speed,
//			"heading": pos.heading != undefined ? pos.heading : pos.coords.heading,
		},
	};
}


/** Debug function */
function fake_pos(data) {
	data = data ? data : _map.getCenter();
	var r = {
		"timestamp": new Date().toJSON(),
		"latlng": data,
		"coords": {
			"latitude": data.latitude != undefined ? data.latitude : data.lat,
			"longitude": data.longitude != undefined ? data.longitude : data.lon,
			"accuracy": data.accuracy != undefined ? data.accuracy : 42,
			"altitude": data.altitude != undefined ? data.altitude : 42,
			"altitudeAccuracy": data.altitudeAccuracy != undefined ? data.altitudeAccuracy : 42,
			"speed": data.speed != undefined ? data.speed : 42,
			"heading": data.heading != undefined ? data.heading : 42,
		},
	}
	locationfound(r);
	return r;
}


function upload_track() {
	// Search for track data not yet uploaded
	var _pos_to_sync = sessionStorage.getItem("_pos_to_sync");
	if (!_pos_to_sync) {
		return;
	}
	_pos_to_sync = JSON.parse(_pos_to_sync);
	if (_pos_to_sync.length == 0) {
		return;
	}
	var uploaded_item_count = _pos_to_sync.length;
	var data = {
		'user': get_username(),
		'track': _pos_to_sync,
	};
	$.ajax({
		url: "/track/", // XXX hard-coded WS-url
		type: 'POST',
		data: JSON.stringify(data),
		success: function (response) {
			try {
				var _remaining_pos_to_sync = sessionStorage.getItem("_pos_to_sync");
				if (_remaining_pos_to_sync) {
					_remaining_pos_to_sync = JSON.parse(_remaining_pos_to_sync);
				} else {
					_remaining_pos_to_sync = _pos_to_sync;
				}
				_remaining_pos_to_sync.splice(0, uploaded_item_count);
			} catch (e) {
				console.error(e);
				_remaining_pos_to_sync = [];
			}
			sessionStorage.setItem("_pos_to_sync", JSON.stringify(_remaining_pos_to_sync));
		},
		error: function (response) {
			console.error(response);
			// _is_online = false; // XXX not sure this is relevant
		},
	});
}


var _tmp_trace = null;
function print_user_track(user, opts) {
	var defaultOptions = {
		"limit": 100,
		"distmin": 0.0001,
		"since": new Date(new Date() - (24 * 60 * 60 * 1000)).toJSON(), // last day
	};
	var _opts = $.extend({}, defaultOptions, opts);
	$.ajax({
		url: "/track/" + user, // XXX hard-coded WS-url
		type: 'POST',
		data: _opts,
		success: function (response) {
			var trace = response.trace;
			var range = response.range;
			if (_tmp_trace) {
				_map.removeLayer(_tmp_trace);
				_tmp_trace = null;
			}
			if (!trace || trace.length == 0) {
				console.warn("Empty trace for " + user);
				return;
			}
			console.info("Trace between ", range[0], range[1]);
			_tmp_trace = L.polyline([],{color: 'green'}).addTo(_map);
			$.each(trace, function (i, item) {
				_tmp_trace.addLatLng([this.coords.latitude, this.coords.longitude]);
			});
			_map.fitBounds(_tmp_trace.getBounds());
		},
		fail: function (response) {
			console.error(response);
		},
	});
}


function get_username() {
	var r = localStorage.getItem('username');
	if (!r || r.trim().length == 0) {
		var suggested_username = 'guest' + new String(new Date().getTime()).slice(-4);
		r = prompt("Choisissez un pseudo.", suggested_username);
		if (!r || r.trim().length == 0) {
			r = suggested_username;
		}
		localStorage.setItem('username', r);
		$('#username').val(r);
	}
	return r;
}


function install() {
	var url = location.protocol + "//" + location.host + location.pathname + "manifest.webapp"; // XXX hard-coded URL
	var request = navigator.mozApps.install(url);
	request.onsuccess = function () {
		console.log(this.result);
	};
	request.onerror = function () {
		console.error("Installation failed: " + this.error.name);
	};
}


// XXX Packaged app is still non-functional due to ineffective Ajax queries. TODO make package app able to send Ajax queries
function install_packaged_app() {
	var url = location.protocol + "//" + location.host + location.pathname + "../dist/manifest.webapp"; // XXX hard-coded URL
	var request = navigator.mozApps.installPackage(url);
	request.onsuccess = function () {
		console.log(this.result);
	};
	request.onerror = function () {
		console.error("Installation failed: " + this.error.name);
	};
}


function start_tracking() {
	// ensure username is present in localStorage
	get_username();
	_map.locate({
		watch: true,
		timeout: 300000,
		enableHighAccuracy: true,
	});
	_is_tracking = true;
	$('#btn-toggle-tracking').text('Ne plus partager ma position');
}


function stop_tracking() {
	_map.stopLocate();
	_is_tracking = false;
	$('#btn-toggle-tracking').text('Partager ma position');
}


function prefs_changed(evt) {
	var $this = $(this);
	_prefs[$this.attr('name')] = $this.val();
	// TODO toggle base layer if user changed it
}


function init_header() {
	$('#online-status').text("online?"); // XXX Mostly for debug
	// Version number is to be fetched differently wether the webapp is installed locally
	// or just visited like a classic web site.
	//$('#app-version').text('0.0');
	if (navigator.mozApps && navigator.mozApps.installPackage) {
		var request = navigator.mozApps.getSelf();
		request.onsuccess = function () {
			document.querySelector('#btn-install').style.display = "";
			if (this.result != null) {
				$('#app-version').text(request.result.manifest.version);
				document.querySelector('#btn-install').disabled = true;
			} else {
				var url = location.protocol + "//" + location.host + location.pathname + "manifest.webapp";
				$.getJSON(url, function (response) {
					$('#app-version').text(response.version);
				});
				var req2 = navigator.mozApps.getInstalled();
				req2.onsuccess = function () {
					if (this.result.length > 0) {
						document.querySelector('#btn-install').disabled = true;
					}
				};
			}
		};
		request.onerror = function () {
			var url = location.protocol + "//" + location.host + location.pathname + "manifest.webapp";
			$.getJSON(url, function (response) {
				$('#app-version').text(response.version);
			});
		};
	} else {
		var url = location.protocol + "//" + location.host + location.pathname + "manifest.webapp";
		$.getJSON(url, function (response) {
			$('#app-version').text(response.version);
		});
		// notify("navigator.mozApps is unavailable on your browser/device.");
	}

	// Event handlers
	$('#username').on('change', function () {
		localStorage.setItem('username', $(this).val());
	});
}


function notify(message) {
	alert(message);
}


function toggle_online_status(isonline) {
	if (isonline) {
		$('#online-status').text("");
		_is_online = true;
		upload_track();
	} else {
//		notify("Connexion perdue. Le partage de votre position ne sera effectif que lorsque vous serez à nouveau connecté.");
		$('#online-status').text("[offline]");
		_is_online = false;
	}
}


function init_event_handlers() {
	toggle_online_status(navigator.onLine);
	$(document).on('online', function () {
		toggle_online_status(true);
	});
	$(document).on('offline', function () {
		toggle_online_status(false);
	});

	$(document).on("pageinit", "#otherusers", function() {
		$( "#users-list" ).on("filterablebeforefilter", function (e, data) {
			var $ul = $('#users-list');
			var $input = $(data.input);
			var value = $input.val();
			if (!value || value.trim().length == 0) {
				value = '%';
			}
			$ul.html("<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>");
			$ul.listview("refresh");
			display_users(value);
		});
	});

	$(document).on("pageshow", "#main-page", function () {
		_map.invalidateSize(false);
	});

	$(document).on("pageshow", "#otherusers", function() {
		display_users('%');
		$('#otherusers input').first().get(0).focus()
	});

	$(document).on("click", "#otherusers #users-list li", function() {
		var selected_username = $(this).data('username');
		if (-1 != _users_to_follow.indexOf(selected_username)) {
			return false; // don't add the same user twice
		}
		$('#followed-users').append('<li data-username="' + selected_username + '" data-icon="delete"><a href="#">' + selected_username + '</a></li>')
		_users_to_follow.push(selected_username);
		$(this).remove();
		refresh_tracks();
		$('#followed-users').listview("refresh");
	});

	$(document).on("click", "#followed-users li", function() {
		if ($(this).data('role') == 'list-divider') {
			return false;
		}
		var username = $(this).data('username');
		_users_to_follow.splice(_users_to_follow.indexOf(username), 1);
		$(this).remove();
		refresh_tracks();
	});

	$(document).on("change", "[name=map-layer-name]", function() {
		switch_layer($(this).val());
	});

	$(document).on("click", '#btn-toggle-tracking', function () {
		if (_is_tracking) {
			stop_tracking();
		} else {
			start_tracking();
		}
		history.back();
	});

	$(document).on("click", "#btn-set-current-loc", function (evt) {
		var ll = $(this).parent().data();
		var latlng = new L.LatLng(ll.lat, ll.lng);
		fake_pos(latlng);
		_map.closePopup();
	});

	$(document).on("click", "#btn-set-marker-loc", function (evt) {
		var ll = $(this).parent().data();
		var latlng = new L.LatLng(ll.lat, ll.lng);
		add_poi(latlng);
		_map.closePopup();
	});

}


function display_users(pattern) {
	var $ul = $('#users-list'),
		$input = $('input:first', $ul),
		value = $input.val(),
		html = "";
		$ul.html("");
	$.ajax({
		url: "/user/", // XXX hard-coded WS-url
		type: "POST",
		data: { asking_user: get_username(), q: pattern },
	})
	.then(function (response) {
		$.each( response, function (i, val) {
			if (-1 != _users_to_follow.indexOf(val.username)) {
				return true; // don't list users already followed
			}
			var delta = val.minutesSinceLastPos + "min";
			if (val.minutesSinceLastPos > 60) {
				delta = "plus de " + Math.floor(val.minutesSinceLastPos / 60) + "h";
			}
			html += '<li data-username="' + val.username + '"><a href="#">' + val.username + '<span class="ui-li-count">'+ delta + '</span>'  + "</a></li>";
		});
		$ul.html( html );
		$ul.listview("refresh");
		$ul.trigger("updatelayout");
	});
}


function refresh_tracks() {
	if (_follow_lock != null) {
		clearInterval(_follow_lock);
	}
	if (_users_to_follow.length == 0) {
		return false;
	}
	_follow_lock = setInterval(function () {
		$.ajax({
			url: "/tracks/", // XXX hard-coded WS-url
			type: 'POST',
			data: JSON.stringify({ usernames: _users_to_follow, since: _follow_last_update }),
			success: function (points) {
				var point;
				for (var i = 0; i < points.length; i++) {
					point = points[i];
					// _follow_last_update = Date.parse(point.timestamp);
					_follow_last_update = new Date();
					_refresh_track(point);
				}
				adjust_map_view();
			},
		});
	}, 5000);
}


function _refresh_track(point) {
	var username = point.username;
	var pos = JSON.parse(point.position);
	var track_info = _tracks[username];
	var latlon = L.latLng(pos.coords.latitude, pos.coords.longitude);
	if (!track_info) {
		track_info = {};
		track_info.trace = L.polyline([],{color: 'red', smoothFactor: 3}).addTo(_map);
		track_info.marker = L.circleMarker(
			latlon, {
			color: 'red',
			radius: 10,
		}).addTo(_map);
		_tracks[username] = track_info;
	}
	track_info.trace.addLatLng(latlon);
	track_info.marker.setLatLng(latlon);
	_last_known_locations[username] = latlon;
}


function add_poi(latlng) {
	if (!latlng) {
		latlng = _map.getCenter();
	}
	var poi = L.marker(latlng, {
		draggable: true,
		clickable: true,
	}).addTo(_map);
	poi.on('click', function (evt) {
		_pois.splice(_pois.indexOf(poi), 1);
		_map.removeLayer(poi);
	});
	_pois.push(poi);
}


$(function() {
	init_header();
	setTimeout(init_map,200); // XXX workaround for race conditions that make leaflet compute wrong map dimensions
	if (localStorage.getItem('username')) {
		$('#username').val(localStorage.getItem('username'));
	}
	init_event_handlers();
	$(document).on("change", "#preferences input", prefs_changed);
});
