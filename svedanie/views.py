# -*- coding: utf-8 -*-

import logging ; log = logging.getLogger(__name__)
from pyramid.view import view_config
from pyramid.response import Response
import json, math, os
from datetime import datetime, timedelta
from dateutil import parser as date_parser

from .models import DBSession, Pos
import sqlalchemy as sa
import transaction


@view_config(route_name='pos.save', renderer='json')
def save_position(request):
    data = request.json_body
    user = data.get('user')
    pos = data.get('pos')
    sess = DBSession()
    position_as_json = json.dumps(pos, indent=2)
    ts = date_parser.parse(pos.get('timestamp'))
    sess.add(Pos(username=user, ts=ts, position=position_as_json))
    return {'status': 'ok'}


@view_config(route_name='track', renderer='json')
def get_track(request):
    user = request.matchdict.get('user')
    opts = dict(request.params)
    outputformat = opts.pop('format', 'json')

    iterpos, range = _track(user, **opts)

    if outputformat == 'gpx':
        return _get_track_gpx(iterpos)
    else:
        return {'trace': [e for e in iterpos], 'range': range }

def _get_track_gpx(iterpos):
    data = """<?xml version="1.0"?>
<gpx
 version="1.1"
 creator="Svedanie"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns="http://www.topografix.com/GPX/1/1"
 xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
"""
    # data += """
# <time>%s</time>
# <bounds minlat="42.401051" minlon="-71.126602" maxlat="42.468655" maxlon="-71.102973"/>
# """ % (datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S'))
    data += """
<trk>
 <trkseg>"""
    for e in iterpos:
        data += """
    <trkpt lat="%s" lon="%s">
      <time>%s</time>
    </trkpt>""" % (e.get('coords').get('latitude'), e.get('coords').get('longitude'), e.get('timestamp'))
    data += """
 </trkseg>
</trk>"""

    data += """</gpx>"""
    # return Response(body=data,content_type='application/xml')
    return Response(body=data,content_type='application/gpx+xml',content_disposition='attachment; filename=' + 'output.gpx')


def _track(username, distmin=0.0001, since=None, before=None, limit=1000):
    q = DBSession.query(Pos.position, Pos.ts)\
        .filter(Pos.username==username)
    if since is not None and since:
        since = date_parser.parse(since)
        q = q.filter(Pos.ts >= since)
        if before is not None:
            before = date_parser.parse(before)
            q = q.filter(Pos.ts <= before)
        q = q.order_by(Pos.ts.asc())
        q = q.limit(limit)
        rows = q.all()
    else:
        q = q.order_by(Pos.ts.desc())
        q = q.limit(limit)
        rows = reversed(q.all())
    range = [rows[0].ts, rows[-1].ts] if len(rows) > 0 else []
    return _simplify_path((json.loads(i.position) for i in rows), float(distmin)), range


def _simplify_path(points, distmin=0.0001):
    first = True
    pos = None
    for pos in points:
        if first:
            first = False
            last_pos = pos
            yield last_pos
        dist = _dist(pos, last_pos)
        if dist < distmin:
            continue
        last_pos = pos
        yield last_pos
    if pos is not None:
        yield pos


def _dist(a, b):
    lat1 = a.get('coords').get('latitude')
    lat2 = b.get('coords').get('latitude')
    lon1 = a.get('coords').get('longitude')
    lon2 = b.get('coords').get('longitude')
    return math.sqrt(pow(lat1-lat2, 2) + pow(lon1-lon2, 2))


@view_config(route_name='track.list', renderer='json')
def get_tracks(request):
    data = request.json_body
    usernames = data.get('usernames')
    if not usernames:
        return []
    limit_in_the_past = datetime.utcnow() - timedelta(hours=6)
    since = data.get('since')
    since = date_parser.parse(since) if since is not None else datetime.utcnow()
    q = DBSession.query(Pos)\
        .filter(Pos.username.in_(usernames))\
        .filter(Pos.ts > since)\
        .filter(Pos.ts > limit_in_the_past)\
        .order_by(Pos.ts.desc()).limit(1000)
    return [i for i in reversed(q.all())]


@view_config(route_name='track.save', renderer='json')
def save_track(request):
    data = request.json_body
    user = data.get('user')
    track = data.get('track')
    sess = DBSession()
    sess.rollback()
    transaction.commit()
    for pos in track:
        transaction.begin()
        try:
            position_as_json = json.dumps(pos, indent=2)
            ts = date_parser.parse(pos.get('timestamp'))
            sess.add(Pos(username=user, ts=ts, position=position_as_json))
            sess.flush()
            transaction.commit()
            log.debug("Saved position for %s: %s" % (user, pos.get('timestamp')))
        except Exception as e:
            log.warn("Failed to store position for %s: %s." % (user, pos.get('timestamp')))
            sess.rollback()
    log.debug("%d position(s) saved for user %s" % (len(track), user))
    sess.flush()
    transaction.commit()
    return {'status': 'ok', 'count': len(track)}


@view_config(route_name='users', renderer='json')
def get_users(request):
    asking_user = request.params.get('asking_user', '')
    pattern = request.params.get('q')
    q = DBSession().query(Pos.username, sa.func.max(Pos.ts))\
        .filter(Pos.username != asking_user)
    if pattern:
        q = q.filter(Pos.username.like("%" + pattern + "%"))
    q = q.group_by(Pos.username)\
        .order_by(sa.func.max(Pos.ts).desc())\
        .limit(5)
    now = datetime.utcnow()
    def _row_to_dict(row):
        delta = now - row[1]
        minutes_since_last_pos = int(math.ceil(delta.total_seconds()/60))
        return {
            "username": row[0],
            "ts": row[1],
            "minutesSinceLastPos": minutes_since_last_pos,
        }

    return [_row_to_dict(u) for u in q]


@view_config(route_name='install', renderer='json')
def install_app(request):
    import pkg_resources
    basedir = pkg_resources.require("Svedanie")[0].location
    orig_manifest = os.path.join(basedir, 'svedanie/static/manifest.webapp')
    bundle_filepath = os.path.join(basedir, 'svedanie/dist/svedanie.zip')

    mf = json.load(open(orig_manifest, 'r'))
    mf['size'] = os.path.getsize(bundle_filepath)
    mf['package_path'] = request.static_url('svedanie:dist/svedanie.zip')
    request.response.content_type = 'application/x-web-app-manifest+json'
    return mf
