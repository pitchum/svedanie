from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

import json
class ComplexJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date):
            return obj.isoformat()
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


def __json__(self, *args, **kwargs):
    return { k:v for k,v in self.__dict__.items() if k != '_sa_instance_state' }
Base.__json__ = __json__


class Pos(Base):
    __tablename__ = 'pos'
    username = Column(Text, primary_key=True)
    ts = Column(DateTime, primary_key=True) # UTC-based timestamp
    position = Column(Text)

    def __str__(self):
        return "Pos ({0}, {1:%Y-%m-%d %H:%M:%S})".format(self.username, self.ts)
