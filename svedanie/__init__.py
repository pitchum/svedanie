# -*- coding: utf-8 -*-

import os
from zipfile import ZipFile
from pyramid.config import Configurator
import pkg_resources

from sqlalchemy import engine_from_config
from .models import (
    DBSession,
    Base,
    )

__version__ = pkg_resources.require("Svedanie")[0].version


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings['app_version'] = __version__

    engine = engine_from_config(settings, 'sqlalchemy.')
    init_db(engine)
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config = Configurator(settings=settings)

    # Custom JSON renderer
    config.add_renderer('json', enhanced_json_renderer())
    config.include('pyramid_chameleon') # forward-compatibility with pyramid 1.5

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('dist', 'dist', cache_max_age=3600)
    config.add_route('pos.save', '/pos/')
    config.add_route('track.save', '/track/')
    config.add_route('track.list', '/tracks/')
    config.add_route('track', '/track/{user}')
    config.add_route('users', '/user/')
    config.add_route('install', '/install/manifest.webapp')
    config.scan('svedanie')

    bundle_app()

    return config.make_wsgi_app()


def init_db(engine):
    url = str(engine.url)
    if url.startswith('sqlite:///'):
        import os
        if not os.path.exists(url[10:]):
            DBSession.configure(bind=engine)
            Base.metadata.create_all(engine)


def enhanced_json_renderer():
    '''
    Returns an enhanced Pyramid JSON renderer.
    Enhanced means:

    - datetimes are automatically stringified
    '''
    from pyramid.renderers import JSON
    import datetime

    json_renderer = JSON()
    def datetime_adapter(obj, request):
        """ date format """
        return obj.isoformat()
    json_renderer.add_adapter(datetime.datetime, datetime_adapter)
    return json_renderer


def bundle_app():
    _prev_cwd = os.getcwd()
    basedir = pkg_resources.require("Svedanie")[0].location
    distdir = os.path.join(basedir, 'svedanie', 'dist')
    staticdir = os.path.join(basedir, 'svedanie', 'static')
    bundle_filename = 'svedanie.zip'

    if not os.path.isdir(distdir):
        os.makedirs(distdir)
    zipfilename = os.path.join(distdir, bundle_filename)

    with ZipFile(zipfilename, 'w') as out:
        os.chdir(os.path.join(basedir, 'svedanie'))
        for folder, subfolders, files in os.walk('static'):
            for filename in files:
                if filename == bundle_filename:
                    continue
                out.write('/'.join((folder,filename)))
        os.chdir(os.path.join(basedir, 'svedanie', 'static'))
        out.write("manifest.webapp")
        print("Bundled app into %s" % (zipfilename))
    os.chdir(_prev_cwd) # revert previous chdir()

