# -*- coding: utf-8 -*-

import os, json

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.rst')).read()
MANIFEST = json.loads(open(os.path.join(here, 'svedanie/static/manifest.webapp')).read())


requires = [
    'pyramid',
    'pyramid_chameleon',
    'pyramid_tm',
    'zope.sqlalchemy',
    'sqlalchemy',
    'waitress',
    ]

setup(
    name=MANIFEST.get('name'),
    version=MANIFEST.get('version'),
    description=MANIFEST.get('description'),
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
      "Programming Language :: Python",
      "Framework :: Pyramid",
      "Topic :: Internet :: WWW/HTTP",
      "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
    author=MANIFEST.get('developer').get('name'),
    author_email='',
    url=MANIFEST.get('developer').get('url'),
    keywords='web pyramid geolocation',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    tests_require=requires,
    test_suite="svedanie",
    entry_points="""\
    [paste.app_factory]
    main = svedanie:main
    """,
)

